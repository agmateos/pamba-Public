import { PAMBAMEANPage } from './app.po';

describe('pamba-mean App', () => {
  let page: PAMBAMEANPage;

  beforeEach(() => {
    page = new PAMBAMEANPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
