webpackJsonp([1],{

/***/ "../../../../../src async recursive":
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = "../../../../../src async recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<top-bar class=\"col-md-12 col-no-padding\">Loading...</top-bar>\n<main class=\"col-md-12 col-no-padding\">\n  <left-bar class=\"col-md-2 col-no-padding\"></left-bar>\n  <router-outlet></router-outlet>\n  <!--<main-display class=\"col-md-10 col-no-padding\"></main-display>-->\n</main>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return Producto; });
/* unused harmony export Filtro */
/* unused harmony export FiltroCantidad */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app';
    }
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/app.component.css")]
    })
], AppComponent);

var Producto = (function () {
    function Producto() {
        this.title = "";
        this.price = 0;
        this.provider = 0;
        this.rating = 0;
        this.img = "";
        this.size = 0;
        this.downloads = 0;
        this.extension = "";
        this.tags = [];
        //this.productFile = ""
        this._id = "";
        this._v = "";
        this.categories = [];
    }
    return Producto;
}());

var Filtro = (function () {
    function Filtro() {
    }
    return Filtro;
}());

var FiltroCantidad = (function () {
    function FiltroCantidad() {
    }
    return FiltroCantidad;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_rating__ = __webpack_require__("../../../../ngx-rating/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_rating___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ngx_rating__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_display_main_display_component__ = __webpack_require__("../../../../../src/app/main-display/main-display.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__top_bar_top_bar_component__ = __webpack_require__("../../../../../src/app/top-bar/top-bar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__left_bar_left_bar_component__ = __webpack_require__("../../../../../src/app/left-bar/left-bar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__filtros_busqueda_filtros_busqueda_component__ = __webpack_require__("../../../../../src/app/filtros-busqueda/filtros-busqueda.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__productos_service__ = __webpack_require__("../../../../../src/app/productos.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__paso_parametros_service__ = __webpack_require__("../../../../../src/app/paso-parametros.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__filtros_service__ = __webpack_require__("../../../../../src/app/filtros.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__auth_service__ = __webpack_require__("../../../../../src/app/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__producto_producto_component__ = __webpack_require__("../../../../../src/app/producto/producto.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__filtro_filtro_component__ = __webpack_require__("../../../../../src/app/filtro/filtro.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__filtro_cantidad_filtro_cantidad_component__ = __webpack_require__("../../../../../src/app/filtro-cantidad/filtro-cantidad.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__callback_callback_component__ = __webpack_require__("../../../../../src/app/callback/callback.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__product_detailed_product_detailed_component__ = __webpack_require__("../../../../../src/app/product-detailed/product-detailed.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__profile_view_profile_view_component__ = __webpack_require__("../../../../../src/app/profile-view/profile-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__messages_messages_component__ = __webpack_require__("../../../../../src/app/messages/messages.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__my_products_my_products_component__ = __webpack_require__("../../../../../src/app/my-products/my-products.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__product_row_product_row_component__ = __webpack_require__("../../../../../src/app/product-row/product-row.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__messages_filter_bar_messages_filter_bar_component__ = __webpack_require__("../../../../../src/app/messages-filter-bar/messages-filter-bar.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

























var appRoutes = [
    { path: 'home', component: __WEBPACK_IMPORTED_MODULE_6__main_display_main_display_component__["a" /* MainDisplayComponent */] },
    { path: 'profile', component: __WEBPACK_IMPORTED_MODULE_20__profile_view_profile_view_component__["a" /* ProfileViewComponent */] },
    { path: 'messages', component: __WEBPACK_IMPORTED_MODULE_21__messages_messages_component__["a" /* MessagesComponent */] },
    { path: 'myProducts', component: __WEBPACK_IMPORTED_MODULE_22__my_products_my_products_component__["a" /* MyProductsComponent */] },
    { path: 'callback', component: __WEBPACK_IMPORTED_MODULE_17__callback_callback_component__["a" /* CallbackComponent */] },
    { path: 'product/:id', component: __WEBPACK_IMPORTED_MODULE_18__product_detailed_product_detailed_component__["a" /* ProductDetailedComponent */] },
    { path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    },
];
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_6__main_display_main_display_component__["a" /* MainDisplayComponent */],
            __WEBPACK_IMPORTED_MODULE_7__top_bar_top_bar_component__["a" /* TopBarComponent */],
            __WEBPACK_IMPORTED_MODULE_8__left_bar_left_bar_component__["a" /* LeftBarComponent */],
            __WEBPACK_IMPORTED_MODULE_9__filtros_busqueda_filtros_busqueda_component__["a" /* FiltrosBusquedaComponent */],
            __WEBPACK_IMPORTED_MODULE_14__producto_producto_component__["a" /* ProductoComponent */],
            __WEBPACK_IMPORTED_MODULE_15__filtro_filtro_component__["a" /* FiltroComponent */],
            __WEBPACK_IMPORTED_MODULE_16__filtro_cantidad_filtro_cantidad_component__["a" /* FiltroCantidadComponent */],
            __WEBPACK_IMPORTED_MODULE_17__callback_callback_component__["a" /* CallbackComponent */],
            __WEBPACK_IMPORTED_MODULE_18__product_detailed_product_detailed_component__["a" /* ProductDetailedComponent */],
            __WEBPACK_IMPORTED_MODULE_20__profile_view_profile_view_component__["a" /* ProfileViewComponent */],
            __WEBPACK_IMPORTED_MODULE_21__messages_messages_component__["a" /* MessagesComponent */],
            __WEBPACK_IMPORTED_MODULE_22__my_products_my_products_component__["a" /* MyProductsComponent */],
            __WEBPACK_IMPORTED_MODULE_23__product_row_product_row_component__["a" /* ProductRowComponent */],
            __WEBPACK_IMPORTED_MODULE_24__messages_filter_bar_messages_filter_bar_component__["a" /* MessagesFilterBarComponent */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_19__angular_router__["a" /* RouterModule */].forRoot(appRoutes),
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_3_ngx_rating__["RatingModule"],
            __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormsModule"]
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_10__productos_service__["a" /* ProductosService */], __WEBPACK_IMPORTED_MODULE_12__filtros_service__["a" /* FiltrosService */], __WEBPACK_IMPORTED_MODULE_13__auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_11__paso_parametros_service__["a" /* PasoParametrosService */]],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/auth.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_filter__ = __webpack_require__("../../../../rxjs/add/operator/filter.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_filter___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_filter__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_auth0_js__ = __webpack_require__("../../../../auth0-js/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_auth0_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_auth0_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AuthService = (function () {
    function AuthService(router, http) {
        this.router = router;
        this.http = http;
        this.userURL = 'api/usuario';
        this.auth0 = new __WEBPACK_IMPORTED_MODULE_3_auth0_js___default.a.WebAuth({
            clientID: '2JvZuThzTvquvBLfLpSzMd7A2VYOvXMc',
            domain: 'pamba.eu.auth0.com',
            responseType: 'token id_token',
            audience: 'https://pamba.eu.auth0.com/userinfo',
            redirectUri: 'http://52.29.128.111:3000/callback',
            scope: 'openid'
        });
    }
    AuthService.prototype.login = function () {
        this.auth0.authorize();
    };
    AuthService.prototype.handleAuthentication = function () {
        var _this = this;
        this.auth0.parseHash(function (err, authResult) {
            if (authResult && authResult.accessToken && authResult.idToken) {
                window.location.hash = '';
                _this.setSession(authResult);
                _this.router.navigate(['/home']);
            }
            else if (err) {
                _this.router.navigate(['/home']);
                console.log(err);
            }
        });
    };
    AuthService.prototype.setSession = function (authResult) {
        // Set the time that the access token will expire at
        var expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
        localStorage.setItem('access_token', authResult.accessToken);
        localStorage.setItem('id_token', authResult.idToken);
        localStorage.setItem('expires_at', expiresAt);
    };
    AuthService.prototype.logout = function () {
        // Remove tokens and expiry time from localStorage
        localStorage.removeItem('access_token');
        localStorage.removeItem('id_token');
        localStorage.removeItem('expires_at');
        // Go back to the home route
        this.router.navigate(['/']);
    };
    AuthService.prototype.isAuthenticated = function () {
        // Check whether the current time is past the
        // access token's expiry time
        var expiresAt = JSON.parse(localStorage.getItem('expires_at'));
        return new Date().getTime() < expiresAt;
    };
    AuthService.prototype.getUsuario = function (idUsuario) {
        console.log('idUsuario', idUsuario);
        return this.http.get(this.userURL + '/' + idUsuario);
        //return { id: '000000000000000000000001' };
    };
    AuthService.prototype.getUsuarioOLD = function () {
        return { id: '000000000000000000000001' };
    };
    return AuthService;
}());
AuthService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_http__["b" /* Http */]) === "function" && _b || Object])
], AuthService);

var _a, _b;
//# sourceMappingURL=auth.service.js.map

/***/ }),

/***/ "../../../../../src/app/callback/callback.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/callback/callback.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"loading\">\n  <img src=\"assets/loading.svg\" alt=\"loading\">\n</div>"

/***/ }),

/***/ "../../../../../src/app/callback/callback.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CallbackComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CallbackComponent = (function () {
    function CallbackComponent() {
    }
    CallbackComponent.prototype.ngOnInit = function () {
    };
    return CallbackComponent;
}());
CallbackComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'callback',
        template: __webpack_require__("../../../../../src/app/callback/callback.component.html"),
        styles: [__webpack_require__("../../../../../src/app/callback/callback.component.css")]
    }),
    __metadata("design:paramtypes", [])
], CallbackComponent);

//# sourceMappingURL=callback.component.js.map

/***/ }),

/***/ "../../../../../src/app/filtro-cantidad/filtro-cantidad.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\nul{\n    list-style: none;\n    padding-left: 20px;\n}\n\n.go-btn {\n    border: 0;\n    height: 21px;\n    margin-bottom: -7px;\n    padding: 0;\n    width: 7%;\n}\n\n.filtro-cantidad {\n    padding-left: 5%;\n    padding-right: 5%;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/filtro-cantidad/filtro-cantidad.component.html":
/***/ (function(module, exports) {

module.exports = "<h5 class=\"row\">{{title}}:</h5>\n\n<div class=\"row\" style=\"padding-right: 8%;\">\n  <input class=\"col-md-3 col-no-padding\" type=\"number\" value=\"\" id=\"{{name}}_low\" name=\"low-size\" maxlength=\"9\">\n  <span class=\"col-md-1\">a</span>\n  <input class=\"col-md-3 col-no-padding\" type=\"number\" value=\"\" id=\"{{name}}_high\" name=\"high-size\" maxlength=\"7\">\n  <div class=\"col-md-3 filtro-cantidad\" >\n    <select id=\"{{name}}\" name=\"{{title}}\" class=\"select-units\">  \n            <option value=\"{{opcion}}\" *ngFor=\"let opcion of units; let i = index;\" [attr.data-index]=\"i\" class=\"col-no-padding\">{{opcion}}</option> \n          </select>\n  </div>\n  <input type=\"image\" (click)=\"checkSelected()\" class=\"go-btn col-md-2\" src=\"https://images-eu.ssl-images-amazon.com/images/G/30/shopping_engine/nav2/buttons/btn-gno-go-sm._CB341434323_.png\"\n    align=\"absbottom\" alt=\"Ir\" title=\"Ir\">\n</div>"

/***/ }),

/***/ "../../../../../src/app/filtro-cantidad/filtro-cantidad.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__productos_service__ = __webpack_require__("../../../../../src/app/productos.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__paso_parametros_service__ = __webpack_require__("../../../../../src/app/paso-parametros.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FiltroCantidadComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FiltroCantidadComponent = (function () {
    function FiltroCantidadComponent(pService, ppService) {
        this.pService = pService;
        this.ppService = ppService;
        this.title = 'Name-test';
        this.name = 'Name-test';
    }
    FiltroCantidadComponent.prototype.init = function () {
        this.title = this.fc.title;
        this.name = this.fc.name;
        this.units = this.fc.filtros;
    };
    FiltroCantidadComponent.prototype.checkSelected = function () {
        var _this = this;
        var min = parseFloat(jQuery('#' + this.name + "_low").val());
        var max = parseFloat(jQuery('#' + this.name + "_high").val());
        if (min != null) {
            this.min = min;
        }
        else {
            this.min = -1;
        }
        if (max != null) {
            this.max = max;
        }
        else {
            this.max = -1;
        }
        this.pService.setFiltros(this.name, { min: this.min, max: this.max }).subscribe(function (res) { _this.ppService.setProductos(res.json()); });
    };
    FiltroCantidadComponent.prototype.ngOnInit = function () {
        this.init();
    };
    return FiltroCantidadComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], FiltroCantidadComponent.prototype, "fc", void 0);
FiltroCantidadComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'filtro-cantidad',
        template: __webpack_require__("../../../../../src/app/filtro-cantidad/filtro-cantidad.component.html"),
        styles: [__webpack_require__("../../../../../src/app/filtro-cantidad/filtro-cantidad.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__productos_service__["a" /* ProductosService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__productos_service__["a" /* ProductosService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__paso_parametros_service__["a" /* PasoParametrosService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__paso_parametros_service__["a" /* PasoParametrosService */]) === "function" && _b || Object])
], FiltroCantidadComponent);

var _a, _b;
//# sourceMappingURL=filtro-cantidad.component.js.map

/***/ }),

/***/ "../../../../../src/app/filtro/filtro.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/filtro/filtro.component.html":
/***/ (function(module, exports) {

module.exports = "<h5>\n  {{title}}:</h5>\n\n<div *ngFor=\"let opcion of filtros; let i = index\" [attr.data-index]=\"i\" class=\"checkbox\" style=\"margin-left: 8%\">\n    <label>\n    <input id= \"{{name}}_{{opcion}}\" type=\"checkbox\" value=\"\" (click)=\"checkSelected(opcion)\">\n    {{opcion}}\n    </label>\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/filtro/filtro.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__productos_service__ = __webpack_require__("../../../../../src/app/productos.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__paso_parametros_service__ = __webpack_require__("../../../../../src/app/paso-parametros.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FiltroComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FiltroComponent = (function () {
    function FiltroComponent(pService, ppService) {
        this.pService = pService;
        this.ppService = ppService;
        this.title = 'Name-test';
        this.name = 'Name-test';
        this.fSeleccionados = [];
    }
    FiltroComponent.prototype.checkSelected = function (filtro) {
        var _this = this;
        if (jQuery('#' + this.name + "_" + filtro).is(":checked")) {
            this.fSeleccionados.push(filtro);
        }
        else {
            var index = this.fSeleccionados.indexOf(filtro);
            this.fSeleccionados.splice(index, 1);
        }
        var aProductos;
        this.pService.setFiltros(this.name, this.fSeleccionados).subscribe(function (res) { _this.ppService.setProductos(res.json()); });
        //this.pService.setFiltros(this.name, this.fSeleccionados).subscribe(res => {aProductos = res.json()});
        //this.ppService.setProductos(aProductos);
    };
    FiltroComponent.prototype.init = function () {
        this.title = this.f.title;
        this.name = this.f.name;
        this.filtros = this.f.filtros;
        this.type = this.f.type;
    };
    FiltroComponent.prototype.ngOnInit = function () {
        console.log("filtro", this.f);
        this.init();
    };
    return FiltroComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], FiltroComponent.prototype, "f", void 0);
FiltroComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'filtro',
        template: __webpack_require__("../../../../../src/app/filtro/filtro.component.html"),
        styles: [__webpack_require__("../../../../../src/app/filtro/filtro.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__productos_service__["a" /* ProductosService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__productos_service__["a" /* ProductosService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__paso_parametros_service__["a" /* PasoParametrosService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__paso_parametros_service__["a" /* PasoParametrosService */]) === "function" && _b || Object])
], FiltroComponent);

var _a, _b;
//# sourceMappingURL=filtro.component.js.map

/***/ }),

/***/ "../../../../../src/app/filtros-busqueda/filtros-busqueda.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.filtros-background {\n    background-color:lightgrey;\n    margin-left: 0px;\n}\n\nul{\n    list-style: none;\n    padding-left: 20px;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/filtros-busqueda/filtros-busqueda.component.html":
/***/ (function(module, exports) {

module.exports = "<section>\n  <h3 class=\"col-md-12\">Filtrar por: </h3>\n  <ul> \n    <li *ngFor=\"let f of aFiltros;let i = index\">\n      <filtro *ngIf=\"f.type == 'check'\" [attr.data-index]=\"i\" [f]=\"f\"></filtro>\n      <filtro-cantidad *ngIf=\"f.type == 'cantidad'\" [attr.data-index]=\"i\" [fc]=\"f\"></filtro-cantidad>\n      <!-- <for each>  <indice> <inyectar indice> <inyectar f (filtro)>-->\n    </li>   \n    <!--<li>\n      <filtro *ngFor=\"let f of aFiltros;let i = index\" [attr.data-index]=\"i\" [f]=\"f\" class=\"\"></filtro>\n      <!-- <for each>  <indice> <inyectar indice> <inyectar f (filtro)>->\n    </li>\n    <li>\n      <filtro-cantidad *ngFor=\"let fc of aFiltrosCantidad;let i = index\" [attr.data-index]=\"i\" [fc]=\"fc\" class=\"\"></filtro-cantidad>\n    </li>-->\n    <!--li>\n      <h5>Tags:</h5>\n      <table>\n        <tbody>\n          <tr>\n            <td width=\"87%\">\n              <input class=\"archivo-input\" type=\"text\" value=\"\" id=\"low-size\" name=\"low-size\" maxlength=\"9\" style=\"width: 100%;\">\n            </td>\n            <td><input style=\"margin-left: 37px; margin-right: 56px;\" type=\"image\" class=\"go-btn\" src=\"https://images-eu.ssl-images-amazon.com/images/G/30/shopping_engine/nav2/buttons/btn-gno-go-sm._CB341434323_.png\"\n                align=\"absbottom\" alt=\"Ir\" title=\"Ir\"></td>\n          </tr>\n        </tbody>\n      </table>\n    </li-->\n  </ul>\n</section>"

/***/ }),

/***/ "../../../../../src/app/filtros-busqueda/filtros-busqueda.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__filtros_service__ = __webpack_require__("../../../../../src/app/filtros.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FiltrosBusquedaComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FiltrosBusquedaComponent = (function () {
    function FiltrosBusquedaComponent(fService) {
        this.fService = fService;
        this.aFiltros = [];
        this.aFiltrosCantidad = [];
    }
    FiltrosBusquedaComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.fService.getFiltros().subscribe(function (res) {
            // for (let filtro of res.json()) {
            //   if (filtro.type = "check") {
            //     this.aFiltros.push(filtro);
            //   }else{
            //     this.aFiltrosCantidad.push(filtro);        
            //   }
            // }
            _this.aFiltros = res.json();
        });
        //this.fService.getFiltrosCantidad().subscribe(res => { this.aFiltrosCantidad = res.json() });
    };
    return FiltrosBusquedaComponent;
}());
FiltrosBusquedaComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'filtros-busqueda',
        template: __webpack_require__("../../../../../src/app/filtros-busqueda/filtros-busqueda.component.html"),
        styles: [__webpack_require__("../../../../../src/app/filtros-busqueda/filtros-busqueda.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__filtros_service__["a" /* FiltrosService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__filtros_service__["a" /* FiltrosService */]) === "function" && _a || Object])
], FiltrosBusquedaComponent);

var _a;
//# sourceMappingURL=filtros-busqueda.component.js.map

/***/ }),

/***/ "../../../../../src/app/filtros.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FiltrosService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FiltrosService = (function () {
    function FiltrosService(http) {
        this.http = http;
        this.filtrosURL = 'api/filtros';
        this.filtrosCantidadURL = 'api/filtros-cantidad';
    }
    FiltrosService.prototype.getFiltros = function () {
        return this.http.get(this.filtrosURL);
    };
    FiltrosService.prototype.getFiltro = function (id) {
        return this.http.get(this.filtrosURL + '/' + id);
    };
    FiltrosService.prototype.getFiltrosCantidad = function () {
        return this.http.get(this.filtrosCantidadURL);
    };
    FiltrosService.prototype.getFiltroCantidad = function (id) {
        return this.http.get(this.filtrosCantidadURL + '/' + id);
    };
    return FiltrosService;
}());
FiltrosService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object])
], FiltrosService);

var _a;
//# sourceMappingURL=filtros.service.js.map

/***/ }),

/***/ "../../../../../src/app/left-bar/left-bar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".background-left-bar{\n    background-color: lightgrey;\n    height:94vh;\n}\n\n.menu-lateral-title{\n    padding: 2%;\n    margin: 0;\n}\n\n.no-border{\n    border-radius: 0px;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/left-bar/left-bar.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"background-left-bar\">\n    <h3 class=\"menu-lateral-title\">{{title}}</h3>\n    <ul class=\"nav nav-pills nav-stacked\">\n        <li role=\"presentation\" [ngClass]=\"{'active': currentView=='home'}\" (click)=\"navigate('home')\"><a class=\"no-border\">Pamba-market</a></li><!--class=\"active\"-->\n        <li role=\"presentation\" [ngClass]=\"{'active': currentView=='profile'}\" (click)=\"navigate('profile')\"><a class=\"no-border\">Perfil</a></li>\n        <li role=\"presentation\" [ngClass]=\"{'active': currentView=='messages'}\" (click)=\"navigate('messages')\"><a class=\"no-border\">Mensajes</a></li>\n        <li role=\"presentation\" [ngClass]=\"{'active': currentView=='myProducts'}\" (click)=\"navigate('myProducts')\"><a class=\"no-border\">Mis productos</a></li>\n    </ul>\n    <filtros-busqueda class=\"col-12\"></filtros-busqueda>\n     <messages-filter-bar class=\"col-12\"></messages-filter-bar>\n<section>\n"

/***/ }),

/***/ "../../../../../src/app/left-bar/left-bar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeftBarComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LeftBarComponent = (function () {
    function LeftBarComponent(router) {
        this.router = router;
        this.title = "Menu principal";
        this.currentView = "home";
    }
    LeftBarComponent.prototype.navigate = function (view) {
        this.currentView = view;
        this.router.navigate(['/' + view]);
    };
    LeftBarComponent.prototype.ngOnInit = function () {
    };
    return LeftBarComponent;
}());
LeftBarComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'left-bar',
        template: __webpack_require__("../../../../../src/app/left-bar/left-bar.component.html"),
        styles: [__webpack_require__("../../../../../src/app/left-bar/left-bar.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _a || Object])
], LeftBarComponent);

var _a;
//# sourceMappingURL=left-bar.component.js.map

/***/ }),

/***/ "../../../../../src/app/main-display/main-display.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".background-main-display {\n    background-color: #E6E6E6;\n    height: 94vh;\n}\n\n.product{\n    margin-top: 20px; \n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main-display/main-display.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"col-md-10 col-no-padding background-main-display\">\n  <producto *ngFor=\"let p of ppService.getProductos();let i = index\" [attr.data-index]=\"i\" [p]=\"p\" class=\"product\" (click)=\"openProduct(p._id)\"></producto>\n<section>\n"

/***/ }),

/***/ "../../../../../src/app/main-display/main-display.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__productos_service__ = __webpack_require__("../../../../../src/app/productos.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__paso_parametros_service__ = __webpack_require__("../../../../../src/app/paso-parametros.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__auth_service__ = __webpack_require__("../../../../../src/app/auth.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainDisplayComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MainDisplayComponent = (function () {
    function MainDisplayComponent(aService, pService, ppService, router, route) {
        this.aService = aService;
        this.pService = pService;
        this.ppService = ppService;
        this.router = router;
        this.route = route;
        this.categories = '';
    }
    MainDisplayComponent.prototype.openProduct = function (id) {
        this.router.navigate(['/product/' + id]);
    };
    MainDisplayComponent.prototype.ngOnInit = function () {
        var _this = this;
        var aProductos;
        this.pService.getProductos().subscribe(function (res) { _this.ppService.setProductos(res.json()); });
        //this.pService.getProductos().subscribe(res => {aProductos = res.json()});
        this.ppService.setProductos(aProductos);
        this.route.params // (+) converts string 'id' to a number 
            .switchMap(function (params) { return _this.aService.getUsuario('000000000000000000000001'); }).subscribe(function (res) {
            console.log(res.json());
            _this.user = res.json();
            _this.categories = _this.user.categories.join(", ");
            console.log(_this.categories);
            _this.ppService.setCategorias(_this.user.categories);
        });
    };
    return MainDisplayComponent;
}());
MainDisplayComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'main-display',
        template: __webpack_require__("../../../../../src/app/main-display/main-display.component.html"),
        styles: [__webpack_require__("../../../../../src/app/main-display/main-display.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__auth_service__["a" /* AuthService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__productos_service__["a" /* ProductosService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__productos_service__["a" /* ProductosService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__paso_parametros_service__["a" /* PasoParametrosService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__paso_parametros_service__["a" /* PasoParametrosService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* ActivatedRoute */]) === "function" && _e || Object])
], MainDisplayComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=main-display.component.js.map

/***/ }),

/***/ "../../../../../src/app/messages-filter-bar/messages-filter-bar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/messages-filter-bar/messages-filter-bar.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  <!--messages-filter-bar works!-->\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/messages-filter-bar/messages-filter-bar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessagesFilterBarComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MessagesFilterBarComponent = (function () {
    function MessagesFilterBarComponent() {
    }
    MessagesFilterBarComponent.prototype.ngOnInit = function () {
    };
    return MessagesFilterBarComponent;
}());
MessagesFilterBarComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'messages-filter-bar',
        template: __webpack_require__("../../../../../src/app/messages-filter-bar/messages-filter-bar.component.html"),
        styles: [__webpack_require__("../../../../../src/app/messages-filter-bar/messages-filter-bar.component.css")]
    }),
    __metadata("design:paramtypes", [])
], MessagesFilterBarComponent);

//# sourceMappingURL=messages-filter-bar.component.js.map

/***/ }),

/***/ "../../../../../src/app/messages/messages.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/messages/messages.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  messages works!\n</p>\n<!--div class=\"list-group\">\n  <a href=\"#\" class=\"list-group-item\">\n    <div class=\"checkbox\">\n      <label>\n        <input type=\"checkbox\">\n      </label>\n    </div>\n    <span class=\"glyphicon glyphicon-star-empty\"></span><span class=\"name\" style=\"min-width: 120px;\n                                display: inline-block;\">Mark Otto</span> <span class=\"\">Nice work on the lastest version</span>\n    <span class=\"text-muted\" style=\"font-size: 11px;\">- More content here</span> <span class=\"badge\">12:10 AM</span> <span class=\"pull-right\"><span class=\"glyphicon glyphicon-paperclip\">\n                                </span></span></a><a href=\"#\" class=\"list-group-item\">\n  <div class=\"checkbox\">\n    <label>\n      <input type=\"checkbox\">\n    </label>\n  </div>\n  <span class=\"glyphicon glyphicon-star-empty\"></span><span class=\"name\" style=\"min-width: 120px;\n                                        display: inline-block;\">Jason Markus</span> <span class=\"\">This is big title</span>\n  <span class=\"text-muted\" style=\"font-size: 11px;\">- I saw that you had..</span> <span class=\"badge\">12:09 AM</span> <span class=\"pull-right\"><span class=\"glyphicon glyphicon-paperclip\">\n                                        </span></span></a><a href=\"#\" class=\"list-group-item read\">\n  <div class=\"checkbox\">\n    <label>\n      <input type=\"checkbox\">\n    </label>\n  </div>\n  <span class=\"glyphicon glyphicon-star\"></span><span class=\"name\" style=\"min-width: 120px;\n                                                display: inline-block;\">Jane Patel</span> <span class=\"\">This is big title</span>\n  <span class=\"text-muted\" style=\"font-size: 11px;\">- Hi hello how r u ?</span> <span class=\"badge\">11:30 PM</span> <span class=\"pull-right\"><span class=\"glyphicon glyphicon-paperclip\">\n                                                </span></span></a>\n</div-->\n"

/***/ }),

/***/ "../../../../../src/app/messages/messages.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessagesComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MessagesComponent = (function () {
    function MessagesComponent() {
    }
    MessagesComponent.prototype.ngOnInit = function () {
    };
    return MessagesComponent;
}());
MessagesComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-messages',
        template: __webpack_require__("../../../../../src/app/messages/messages.component.html"),
        styles: [__webpack_require__("../../../../../src/app/messages/messages.component.css")]
    }),
    __metadata("design:paramtypes", [])
], MessagesComponent);

//# sourceMappingURL=messages.component.js.map

/***/ }),

/***/ "../../../../../src/app/my-products/my-products.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.my-products-header{\n    margin-top: 1.5%;\n    margin-left: 1%;\n}\n\n.my-products-table{\n    width: 95%;\n    margin-left: 2.5%;\n}\n\n.table-row{\n    cursor: pointer;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/my-products/my-products.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"col-md-10 col-no-padding background-main-display\">\n  <h4 class=\"my-products-header\">\n    Mis productos:\n  </h4>\n  <hr>\n  <table class=\"table table-hover table-bordered my-products-table\">\n    <thead>\n      <tr>\n        <th>Título del producto</th>\n        <th>Proveedor</th>\n        <th>Valoración actual</th>\n        <th>Tamaño</th>\n        <th>Extensión</th>\n        <th>Fecha de compra</th>\n      </tr>\n    </thead>\n    <tbody>\n\n      <tr product-row *ngFor=\"let p of productos; let i = index\" [attr.data-index]=\"i\" [p]=\"p\" (click)=\"openProduct(p.producto._id)\" class=\"table-row\">\n      </tr>\n    </tbody>\n  </table>\n  <hr>\n</section>"

/***/ }),

/***/ "../../../../../src/app/my-products/my-products.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__productos_service__ = __webpack_require__("../../../../../src/app/productos.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_service__ = __webpack_require__("../../../../../src/app/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyProductsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MyProductsComponent = (function () {
    function MyProductsComponent(pService, auth, router) {
        this.pService = pService;
        this.auth = auth;
        this.router = router;
        this.productos = [];
    }
    MyProductsComponent.prototype.openProduct = function (id) {
        this.router.navigate(['/product/' + id]);
    };
    MyProductsComponent.prototype.ngOnInit = function () {
        this.getProductoExtended();
    };
    MyProductsComponent.prototype.getProductoExtended = function () {
        var _this = this;
        this.pService.getProductoExtended(this.auth.getUsuarioOLD()['id']).subscribe(function (res) { return _this.productos = res.json(); });
    };
    return MyProductsComponent;
}());
MyProductsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-my-products',
        template: __webpack_require__("../../../../../src/app/my-products/my-products.component.html"),
        styles: [__webpack_require__("../../../../../src/app/my-products/my-products.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__productos_service__["a" /* ProductosService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__productos_service__["a" /* ProductosService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__auth_service__["a" /* AuthService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]) === "function" && _c || Object])
], MyProductsComponent);

var _a, _b, _c;
//# sourceMappingURL=my-products.component.js.map

/***/ }),

/***/ "../../../../../src/app/paso-parametros.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PasoParametrosService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PasoParametrosService = (function () {
    function PasoParametrosService() {
        this.aCategorias = [];
    }
    PasoParametrosService.prototype.getProductos = function () {
        console.log(this.aProductos);
        return this.aProductos;
    };
    PasoParametrosService.prototype.setProductos = function (productos) {
        this.aProductos = productos;
        this.orderProductos();
    };
    //producto.categories
    PasoParametrosService.prototype.orderProductos = function () {
        var size = this.aCategorias.length;
        var newAProductos = [];
        console.log(this.aCategorias);
        while (size >= 0) {
            var _loop_1 = function (pIndex) {
                var producto = this_1.aProductos[pIndex];
                var arrIntersection = this_1.aCategorias.filter(function (value) { return -1 !== producto.categories.indexOf(value); }); //hayamos intersección de los dos arrays
                if (arrIntersection.length == size) {
                    newAProductos.push(producto);
                }
            };
            var this_1 = this;
            for (var pIndex in this.aProductos) {
                _loop_1(pIndex);
            }
            size--;
        }
        this.aProductos = newAProductos;
    };
    /*
    let categoriasCoincidentes = 0
    for(let categorie of producto.categories){
      if(producto.categories){
        categoriasCoincidentes++;
      }
    }*/
    PasoParametrosService.prototype.getCategorias = function () {
        console.log(this.aCategorias);
        return this.aCategorias;
    };
    PasoParametrosService.prototype.setCategorias = function (categorias) {
        this.aCategorias = categorias;
        this.orderProductos();
    };
    return PasoParametrosService;
}());
PasoParametrosService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], PasoParametrosService);

//# sourceMappingURL=paso-parametros.service.js.map

/***/ }),

/***/ "../../../../../src/app/product-detailed/product-detailed.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.details-list{\n    list-style-type: none;\n}\n.details-element{\n    margin: 5%;\n    display: inline\n}\n\n.descripcion{\n    /*border: 1px;*/\n    min-height: 200px;\n    background-color: #F2F2F2;\n    border-color: black;\n    border-radius: 25px;\n    border: 1px;\n    border-style: solid;\n    margin-top: 1%;\n    margin-bottom: 1%;\n    /*margin: 1%;*/\n}\n\n.descripcion-content{\n    margin-left: 10px;\n}\n\n.back-button{\n    color: #fff !important;\n    text-align: center;\n    cursor: pointer;\n    font-size: 1em;\n    font-weight: bold !important;\n    border-radius: 4px;\n    background-color: #56aa29;\n    line-height: 18px;\n    margin-bottom: 0;\n    vertical-align: middle;\n    border: 0px;\n    width: 45%;\n    margin-top: 12%;\n}\n\n.demo-button{\n    padding: 1em;\n    color: #fff !important;\n    text-align: center;\n    cursor: pointer;\n    font-size: 1em;\n    font-weight: bold !important;\n    border-radius: 4px;\n    background-color: #56aa29;\n    line-height: 18px;\n    margin-bottom: 0;\n    vertical-align: middle;\n    border: 0px;\n    width: 45%;\n    /*margin-top: 7%;*/\n}\n\n.purchase-button{\n    padding: 1em;\n    color: #fff !important;\n    text-align: center;\n    cursor: pointer;\n    font-size: 1em;\n    font-weight: bold !important;\n    border-radius: 4px;\n    background-color: #3476d9;\n    line-height: 18px;\n    margin-bottom: 0;\n    vertical-align: middle;\n    border: 0px;\n    width: 45%;\n    /*margin-top: 7%;*/\n}\n\n.review-header{\n    margin-top: 1%;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/product-detailed/product-detailed.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-10 col-no-padding\">\n  <div class=\"col-md-12\">\n    <div id=\"producto_Name\" class=\"col-md-6\">\n      <h2 class=\"col-md-12\">Nombre: <span class=\"\">{{producto.title}}</span> </h2>\n    </div>\n    <div id=\"producto_Price\" class=\"col-md-5\">\n      <h2 class=\"col-md-12\">Precio:<span class=\"\">{{producto.price}}€</span> </h2>\n    </div>\n\n    <div class=\"remove-btn header-btn pull-right\">\n      <a href=\"#\" title=\"Close this bar\" class=\"fa fa-fw fa-times\"></a>\n      <!--!!!!!!!!!!!!!!-->\n    </div>\n    <!--<div class=\" col-md-1 col-no-padding\">\n      <button class=\"back-button\" (click)=\"close()\">\n      <h4>Volver</h4>\n      </button>\n    </div>-->\n  </div>\n  <img id=\"producto_img\" src='http://www.jmozeley.com/wp-content/uploads/2014/02/master-suite-2.jpg' class=\"col-md-6\">\n  <div id=\"producto_details\" class=\"col-md-6\">\n    <h3>Detalles:</h3>\n    <ul class=\"details-list\">\n      <li class=\"details-element\">\n        <h4>Proveedor: <span>{{producto.provider}}</span></h4>\n      </li>\n      <li class=\"details-element\">\n        <h4>\n          Tamaño: <span>{{producto.size}}</span>\n        </h4>\n\n      </li>\n      <li class=\"details-element\">\n        <h4>\n          Formato: <span>{{producto.extension}}</span>\n        </h4>\n      </li>\n      <li class=\"details-element\">\n        <h4>\n          Descargas: <span>{{producto.downloads}}</span>\n        </h4>\n      </li>\n      <li class=\"details-element\">\n        <div class=\"col-sm-5\" >\n          <rating [(ngModel)]=\"producto.rating\" [titles]=\"['one', 'two', 'three', 'four', 'five']\">\n          </rating>\n        </div>\n      </li>\n      <!--<li class=\"details-element\">\n        <h4>\n          Score de afinidad con el usuario: <span>{{indiceAfinidad}} %</span>\n        </h4>\n      </li>-->\n    </ul>\n    <h4 style=\"margin-top:18%\">\n      Score de afinidad con el usuario: <span>{{indiceAfinidad}} %</span>\n    </h4>\n  </div>\n  <div class=\"col-md-12\">\n    <div class=\"descripcion\">\n          <h4 class=\"descripcion-content\">Descripción:</h4>\n          <p class=\"descripcion-content\">///////Descripción example/////</p>\n    </div>\n  </div>\n  <div class=\"col-md-12 col-no-padding\">\n    <div class=\"col-md-offset-3 col-md-3 col-no-padding\">\n      <button class=\"purchase-button\" (click)=\"buyProduct()\">\n        <h4>Adquirir producto</h4>\n      </button>\n    </div>\n    <div class=\"col-md-offset-1  col-md-3 col-no-padding\">\n      <button class=\"demo-button\" (click)=\"close()\">\n      <h4>DEMO</h4>\n      </button>\n    </div>\n  </div>\n  <div class=\"col-md-12 col-no-padding review-header\">\n    <h4 class=\"\">Reviews:\n    </h4>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/product-detailed/product-detailed.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__productos_service__ = __webpack_require__("../../../../../src/app/productos.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_file_saver__ = __webpack_require__("../../../../file-saver/FileSaver.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_file_saver___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_file_saver__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_switchMap__ = __webpack_require__("../../../../rxjs/add/operator/switchMap.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_switchMap___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_switchMap__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductDetailedComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var slugify = __webpack_require__("../../../../slugify/index.js");
var ProductDetailedComponent = (function () {
    function ProductDetailedComponent(pService, router, route) {
        this.pService = pService;
        this.router = router;
        this.route = route;
        this.producto = new __WEBPACK_IMPORTED_MODULE_2__app_component__["b" /* Producto */]();
        this.indiceAfinidad = 0;
    }
    ProductDetailedComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log(this.route.params);
        console.log(2);
        var user = 'testUser';
        //this.pService.getRecomendacion(1111111111, 999999);
        this.route.params // (+) converts string 'id' to a number 
            .switchMap(function (params) { return _this.pService.getRecomendacion(params['id'], user); }).subscribe(function (res) {
            _this.indiceAfinidad = Math.trunc(res.json() * 10);
        });
        this.route.params // (+) converts string 'id' to a number 
            .switchMap(function (params) { return _this.pService.getProducto(params['id']); }).subscribe(function (res) { return _this.producto = res.json(); });
    };
    ProductDetailedComponent.prototype.close = function () {
        this.router.navigate(['/home']);
    };
    ProductDetailedComponent.prototype.buyProduct = function () {
        var _this = this;
        //var idProduct = this.producto._id;
        //this.pService.downloadFile(idProduct)
        this.pService.downloadFile(this.producto._id).subscribe(function (blob) {
            if (_this.producto.extension.length > 0) {
                var extension = '.' + _this.producto.extension;
            }
            else {
                var extension = "";
            }
            var nombre = slugify(_this.producto.title) + extension;
            //console.log(nombre);
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_4_file_saver__["saveAs"])(blob, nombre);
        });
    };
    return ProductDetailedComponent;
}());
ProductDetailedComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-product-detailed',
        template: __webpack_require__("../../../../../src/app/product-detailed/product-detailed.component.html"),
        styles: [__webpack_require__("../../../../../src/app/product-detailed/product-detailed.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__productos_service__["a" /* ProductosService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__productos_service__["a" /* ProductosService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* ActivatedRoute */]) === "function" && _c || Object])
], ProductDetailedComponent);

var _a, _b, _c;
//# sourceMappingURL=product-detailed.component.js.map

/***/ }),

/***/ "../../../../../src/app/product-row/product-row.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/product-row/product-row.component.html":
/***/ (function(module, exports) {

module.exports = "  <!-- <td>John</td>\n  <td>Doe</td>\n  <td>john@example.com</td>\n  <td>1KB</td>\n  <td>.sql</td>\n  <td>29/11/1991</td> -->\n\n\n  <td>{{title}}</td>\n  <td>{{provider}}</td>\n  <td>{{rating}}</td>\n  <td>{{size}}</td>\n  <td>{{extension}}</td>\n  <td>{{date}}</td>\n"

/***/ }),

/***/ "../../../../../src/app/product-row/product-row.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductRowComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProductRowComponent = (function () {
    function ProductRowComponent() {
        this.title = "";
        this.provider = "";
        this.rating = "";
        this.size = "";
        this.extension = "";
        this.date = "";
    }
    ProductRowComponent.prototype.init = function () {
        this.title = this.p.producto.title;
        this.provider = this.p.producto.provider;
        this.rating = this.p.producto.rating;
        this.size = this.p.producto.size;
        this.extension = this.p.producto.extension;
        this.date = this.p.fecha;
    };
    ProductRowComponent.prototype.ngOnInit = function () {
        this.init();
    };
    return ProductRowComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], ProductRowComponent.prototype, "p", void 0);
ProductRowComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: '[product-row]',
        template: __webpack_require__("../../../../../src/app/product-row/product-row.component.html"),
        styles: [__webpack_require__("../../../../../src/app/product-row/product-row.component.css")]
    }),
    __metadata("design:paramtypes", [])
], ProductRowComponent);

//# sourceMappingURL=product-row.component.js.map

/***/ }),

/***/ "../../../../../src/app/producto/producto.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".producto {\n    background-color:white;\n    height: 120px;\n    border-radius: 10px;\n}\n\n.product-icon{\n    height: 100%;\n    background-image: url(\"https://cdn0.iconfinder.com/data/icons/creative-nerds-wooden-icons/128/p.png\");\n    /*background-image: url(\"/src/img/pamba-icon.png\");*/\n    background-repeat: no-repeat;\n}\n\n.gradient {\n  height: 33vh;\n  background-color: grey;\n  background-image:\n    radial-gradient(\n      ellipse closest-side,\n      #fff,\n      #ccc\n    );\n}\n\n.wrap {\n  width: 100%;\n  position: relative;\n  margin: 0 auto 0 auto;\n  top: 50%;\n  transform: translateY(-50%);\n}\n@media (min-width:800px) {\n  .wrap {\n  width: 100%;\n  }\n}\n\n.card {\n  position: relative;\n  background-color: #fff;\n  border-radius: 4px;\n  box-shadow: 0px 1px 5px #999;\n  overflow: hidden;\n  z-index: 10;\n}\n.image {\n  background-image: url('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS-e8fjtaU-avoKqFs-FPUFkC9VF9mibSUQVMc-8eqMEISvCa0K');\n  margin-left: -42px;\n  background-size: cover;\n  position: center;\n  background-repeat: no-repeat;\n  min-height: 300px;\n  transition: all .5s ease-in-out;\n  &:hover {\n    transform: rotate(-3deg) scale(1.1);\n    -webkit-transform: rotate(-3deg) scale(1.1);\n  }\n    \n}\n\n.image2 {\n  background-image: url('http://www.jmozeley.com/wp-content/uploads/2014/02/master-suite-2.jpg');\n  background-size: cover;\n  position: center;\n  background-repeat: no-repeat;\n  min-height: 300px;\n  transition: all .5s ease-in-out;\n  &:hover {\n    transform: rotate(-3deg) scale(1.1);\n    -webkit-transform: rotate(-3deg) scale(1.1);\n  }\n    \n}\n\n.price {\n  position: absolute;\n  top: 0;\n  right: 0;\n  width: auto;\n  color: #eee;\n  padding: 5%;\n  background-color: #333;\n  background-color: rgba(76, 166, 255, .9);\n  font-size: initial;\n  z-index: 10;\n  span {\n    font-size: 0.6em;\n  }\n}\n\n.padding-lateral-0 {\n  padding-left:0%;\n  padding-right:0%;\n}\n\n.description {\n  position: absolute;\n  z-index: 10;\n  bottom: 0;\n  width: 100%;\n  color: #eee;\n  padding: 10px 20px 0px 20px;\n  background-color: #333;\n  background-color: rgba(0, 0, 0, .7);\n  ul {padding: 0;}\n  li {\n    list-style: none;\n    display: inline-block;\n    margin-right: 20px;\n  }  \n  p {\n    font-size: 12px;\n    padding-top: 10px;\n    text-align: center;\n  }\n  .glyphicon {\n    font-size: 20px;\n    text-align: center;\n}\n}\n\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/producto/producto.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"gradient col-md-4\" style=\"padding-bottom: 1px;\">\n  <div class=\"wrap\" style=\"height: 31vh;\">\n    <div class=\"card\" style=\"height:inherit;\">\n      <div class=\"price\">{{price}}<span class=\"glyphicon glyphicon-eur\" aria-hidden=\"true\"></span></div>\n      <div class=\"image\"></div>\n      <div class=\"description\">\n        <div class=\"row\">\n          <div class=\"col-sm-4\" style=\"padding-left:4%; padding-right:4%;\">\n            <h4>{{title}}</h4>\n          </div>\n          <div class=\"col-sm-4\" style=\"padding-left:4%; padding-right:4%;\">\n            <h5>Proveedor: {{provider}}</h5>\n          </div>\n          <div class=\"col-sm-4 padding-lateral-0\">\n            <rating [(ngModel)]=\"rating\" [titles]=\"['one', 'two', 'three', 'four', 'five']\" style=\"font-size: x-small;\">\n            </rating>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/producto/producto.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductoComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProductoComponent = (function () {
    function ProductoComponent() {
        this.img = "";
        this.title = 'Name-test';
        this.price = 0;
        this.provider = "provider-test";
        this.rating = 0;
        // console.log(this.p);
    }
    ProductoComponent.prototype.init = function () {
        this.title = this.p.title;
        this.price = this.p.price;
        this.provider = this.p.provider;
        this.rating = this.p.rating;
    };
    ProductoComponent.prototype.ngOnInit = function () {
        console.log("producto", this.p);
        this.init();
    };
    return ProductoComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], ProductoComponent.prototype, "p", void 0);
ProductoComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'producto',
        template: __webpack_require__("../../../../../src/app/producto/producto.component.html"),
        styles: [__webpack_require__("../../../../../src/app/producto/producto.component.css")]
    }),
    __metadata("design:paramtypes", [])
], ProductoComponent);

//# sourceMappingURL=producto.component.js.map

/***/ }),

/***/ "../../../../../src/app/productos.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductosService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProductosService = (function () {
    function ProductosService(http) {
        this.http = http;
        this.productosURL = 'api/productos';
        this.recomendadorURL = 'api/recomendador';
        this.productoExtendedURL = 'api/productoExtended';
        this.descargaURL = 'api/downloadProduct';
        this.filtros = {};
    }
    /*destildear(str) {
      var tittles = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç";
      var original = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc";
      for (var i = 0; i < tittles.length; i++) {
        str = str.replace(tittles.charAt(i), original.charAt(i)).toLowerCase();
      };
      return str;
    }*/
    ProductosService.prototype.getProductos = function () {
        return this.http.get(this.productosURL, {
            params: this.filtros
        });
        //.map(this.extractData)
        //.catch(this.handleError);
    };
    ProductosService.prototype.getProducto = function (id) {
        var params = new URLSearchParams();
        for (var key in this.filtros) {
            params.set(key, this.filtros[key]);
        }
        return this.http.get(this.productosURL + '/' + id, {
            search: params
        });
        //.map(this.extractData)
        //.catch(this.handleError);
    };
    ProductosService.prototype.getRecomendacion = function (idProducto, idUsuario) {
        return this.http.get(this.recomendadorURL + '/' + idProducto + '/' + idUsuario);
        //.map(this.extractData)
        //.catch(this.handleError);
    };
    ProductosService.prototype.getProductoExtended = function (idUsuario) {
        return this.http.get(this.productoExtendedURL + '/' + idUsuario, {});
    };
    ProductosService.prototype.downloadFile = function (id) {
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* ResponseContentType */].Blob });
        return this.http.get(this.descargaURL + '/' + id, options)
            .map(function (res) { return res.blob(); })
            .catch(this.handleError);
    };
    /*getProductoURL(id: number){
      return
  
    }*/
    ProductosService.prototype.setFiltros = function (key, filtros) {
        this.filtros[key] = filtros;
        console.log("filtros:", this.filtros);
        return this.getProductos();
    };
    ProductosService.prototype.extractData = function (res) {
        var body = res.json();
        console.log(body);
        return body.data || [];
    };
    ProductosService.prototype.handleError = function (error) {
        // In a real world app, you might use a remote logging infrastructure
        var errMsg;
        if (error instanceof __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* Response */]) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(errMsg);
    };
    return ProductosService;
}());
ProductosService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object])
], ProductosService);

var _a;
//# sourceMappingURL=productos.service.js.map

/***/ }),

/***/ "../../../../../src/app/profile-view/profile-view.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.profile-view{\npadding-top:1.5%; \n}\n\n.profile-img{\n    width:90%;\n    height:90%;\n}\n\n.profile-list{\n    list-style-type: none;\n}\n\n.profile-element{\n    margin: 5%;\n    display: inline\n}\n\n\n.descripcion{\n    /*border: 1px;*/\n    min-height: 200px;\n    background-color: #F2F2F2;\n    border-color: black;\n    border-radius: 25px;\n    border: 1px;\n    border-style: solid;\n    margin-top: 1%;\n    margin-bottom: 1%;\n    /*margin: 1%;*/\n}\n\n.descripcion-content{\n    margin-left: 10px;\n}\n\n.save-button{\n    padding: 1em;\n    color: #fff !important;\n    text-align: center;\n    cursor: pointer;\n    font-size: 1em;\n    font-weight: bold !important;\n    border-radius: 4px;\n    background-color: #3476d9;\n    line-height: 18px;\n    margin-bottom: 0;\n    vertical-align: middle;\n    border: 0px;\n    width: 45%;\n    /*margin-top: 7%;*/\n}\n\n.cancel-button{\n    padding: 1em;\n    color: #fff !important;\n    text-align: center;\n    cursor: pointer;\n    font-size: 1em;\n    font-weight: bold !important;\n    border-radius: 4px;\n    background-color: #8b0000;\n    line-height: 18px;\n    margin-bottom: 0;\n    vertical-align: middle;\n    border: 0px;\n    width: 45%;\n    /*margin-top: 7%;*/\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/profile-view/profile-view.component.html":
/***/ (function(module, exports) {

module.exports = "<!--p>\n  profile-view works!\n</p-->\n\n<div class=\"col-md-10 profile-view\">\n  <span class=\"col-md-4\">\n  <img id=\"user-img\" class = \"profile-img\" src=\"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS9E0nirlTy6GwMWXQ7x3BKe1W7QGRe--JVKnS6MltIAxTwTB7AE_0rJvs\" >\n  </span>\n  <span class=\"col-md-8\">\n\n    <div class=\"form-group row\">\n  <label for=\"example-text-input\" class=\"col-2 col-form-label\">Usuario: </label>\n  <div class=\"col-10\">\n    <input class=\"form-control\" type=\"text\" value=\"TestUser\" id=\"example-text-input\">\n  </div>\n</div>\n\n<div class=\"form-group row\">\n  <label for=\"example-email-input\" class=\"col-2 col-form-label\">Nombre: </label>\n  <div class=\"col-10\">\n    <input class=\"form-control\" type=\"email\" value=\"{{user.user_name}}\" id=\"example-email-input\">\n  </div>\n</div>\n<div class=\"form-group row\">\n  <label for=\"example-url-input\" class=\"col-2 col-form-label\">Apellidos: </label>\n  <div class=\"col-10\">\n    <input class=\"form-control\" type=\"url\" value=\"{{user.user_surname}}\" id=\"example-url-input\">\n  </div>\n</div>\n<div class=\"form-group row\">\n  <label for=\"example-tel-input\" class=\"col-2 col-form-label\">Empresa asociada </label>\n  <div class=\"col-10\">\n    <input class=\"form-control\" type=\"tel\" value=\"NONE\" id=\"example-tel-input\">\n  </div>\n</div>\n<div class=\"form-group row\">\n  <label for=\"example-password-input\" class=\"col-2 col-form-label\">Móvil de contacto: </label>\n  <div class=\"col-10\">\n    <input class=\"form-control\"  value=\"1-(555)-555-5555\" id=\"example-password-input\"><!--type=\"password\"-->\n  </div>\n</div>\n<div class=\"form-group row\">\n  <label for=\"example-number-input\" class=\"col-2 col-form-label\">E-mail: </label>\n  <div class=\"col-10\">\n    <input class=\"form-control\" type=\"email\" value=\"{{user.email}}\" id=\"example-number-input\">\n  </div>\n</div>\n<div class=\"form-group row\">\n  <label for=\"example-datetime-local-input\" class=\"col-2 col-form-label\">Documento de identidad / idFiscal: </label>\n  <div class=\"col-10\">\n    <input class=\"form-control\" type=\"text\" value=\"000000000\" id=\"example-datetime-local-input\">\n  </div>\n</div>\n<div class=\"form-group row\">\n  <label for=\"example-search-input\" class=\"col-2 col-form-label\">Categorías de interés:</label>\n  <div class=\"col-10\">\n    <input class=\"form-control\" type=\"search\" value=\"{{categories}}\" id=\"example-search-input\">\n  </div>\n</div>\n  </span>\n  <div class=\"col-md-12\">\n    <div class=\"descripcion\">\n        <h4 class=\"descripcion-content\">Descripción:</h4>\n        <p class=\"descripcion-content\">///////Descripción example/////</p>\n    </div>\n  </div>\n  <div class=\"col-md-12\">\n    <!--\n    <div><span>Tags tipos de datos: </span><span>resp(tags)</span></div>\n    -->\n  </div>\n  <div class=\"col-md-12 col-no-padding\">\n    <div class=\"col-md-offset-3 col-md-3 col-no-padding\">\n      <button class=\"save-button\">\n        <h4>Guardar cambios</h4>\n      </button>\n    </div>\n    <div class=\"col-md-offset-1 col-md-3 col-no-padding\">\n      <button class=\"cancel-button\">\n        <h4>Cancelar cambios</h4>\n      </button>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/profile-view/profile-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__auth_service__ = __webpack_require__("../../../../../src/app/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_switchMap__ = __webpack_require__("../../../../rxjs/add/operator/switchMap.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_switchMap___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_switchMap__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileViewComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ProfileViewComponent = (function () {
    function ProfileViewComponent(aService, router, route) {
        this.aService = aService;
        this.router = router;
        this.route = route;
        this.categories = '';
    }
    ProfileViewComponent.prototype.ngOnInit = function () {
        /*
        this.route.params // (+) converts string 'id' to a number
        .switchMap((params: Params) => this.aService.getUsuario(params['id'])).subscribe(res => {
          console.log(res.json())
        });
        */
        var _this = this;
        this.route.params // (+) converts string 'id' to a number 
            .switchMap(function (params) { return _this.aService.getUsuario('000000000000000000000001'); }).subscribe(function (res) {
            console.log(res.json());
            _this.user = res.json();
            _this.categories = _this.user.categories.join(", ");
            console.log(_this.categories);
            /*for(let categorie of this.user.categories){
              this.categories += categorie
            }*/
        });
    };
    return ProfileViewComponent;
}());
ProfileViewComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-profile-view',
        template: __webpack_require__("../../../../../src/app/profile-view/profile-view.component.html"),
        styles: [__webpack_require__("../../../../../src/app/profile-view/profile-view.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__auth_service__["a" /* AuthService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* ActivatedRoute */]) === "function" && _c || Object])
], ProfileViewComponent);

var _a, _b, _c;
//# sourceMappingURL=profile-view.component.js.map

/***/ }),

/***/ "../../../../../src/app/top-bar/top-bar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".background-top-bar {\n    padding-top: 10px;\n}\n\n.height-top-bar {    \n    height: 100%;\n}\n.pamba-icon{\n    background-image: url(\"https://cdn0.iconfinder.com/data/icons/creative-nerds-wooden-icons/128/p.png\");\n    /*background-image: url(\"/src/img/pamba-icon.png\");*/\n    background-repeat: no-repeat;\n}\n\n.pamba-icon-img {\n    width: auto;\n    height: 38px;\n    top: -4px;\n    padding-right: 0;\n}\n\n.pamba-title {\n    -ms-flex-item-align: center;\n        -ms-grid-row-align: center;\n        align-self: center;\n    font-size: 20px;\n}\n\n.pamba-buscador{\n    -ms-flex-item-align: center;\n        -ms-grid-row-align: center;\n        align-self: center;\n}\n\n.pamba-botones{\n    -ms-flex-item-align: center;\n        -ms-grid-row-align: center;\n        align-self: center;\n    font-size: 20px;\n}\n\n.no-margin-bot{\n    margin-bottom: 0px;\n}\n\n.botones-registro{\n    margin-top: 0.5%;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/top-bar/top-bar.component.html":
/***/ (function(module, exports) {

module.exports = "<header class=\"navbar navbar-default navbar-static-top no-margin-bot height-top-bar\">\n  <div class=\"container-fluid\" style=\"height:  inherit;\">\n    <div class=\"navbar-header col-md-10 background-top-bar\" style=\"height:  inherit;\">\n      <!--span class=\"pamba-icon col-md-1\"></span-->\n      <img class=\"col-md-1 pamba-icon-img\" src=\"https://cdn0.iconfinder.com/data/icons/creative-nerds-wooden-icons/128/p.png\">\n      <a href=\"/\" class=\"pamba-title col-md-2\">{{title}}</a>\n      <div class=\"pamba-buscador col-md-offset-1 col-md-4\">\n        <div class=\"input-group\">\n          <input type=\"text\" class=\"form-control\" placeholder=\"Search for...\">\n          <span class=\"input-group-btn\">\n            <button class=\"btn btn-default\" type=\"button\">Go!</button>\n          </span>\n        </div>\n      </div>\n    </div>\n    <div class=\"collapse navbar-collapse col-md-2 botones-registro\">\n      <ul class=\"nav navbar-nav\">\n        <li>\n          <button class=\"btn btn-primary btn-margin\" *ngIf=\"!auth.isAuthenticated()\" (click)=\"auth.login()\">\n          Log In / Register\n      </button>\n          <button class=\"btn btn-primary btn-margin\" *ngIf=\"auth.isAuthenticated()\" (click)=\"auth.logout()\">\n          Log Out\n      </button>\n        </li>\n      </ul>\n    </div>\n  </div>\n</header>"

/***/ }),

/***/ "../../../../../src/app/top-bar/top-bar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__auth_service__ = __webpack_require__("../../../../../src/app/auth.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TopBarComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TopBarComponent = (function () {
    function TopBarComponent(auth) {
        this.auth = auth;
        this.title = "Pamba store";
        auth.handleAuthentication();
    }
    TopBarComponent.prototype.ngOnInit = function () {
    };
    return TopBarComponent;
}());
TopBarComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'top-bar',
        template: __webpack_require__("../../../../../src/app/top-bar/top-bar.component.html"),
        styles: [__webpack_require__("../../../../../src/app/top-bar/top-bar.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__auth_service__["a" /* AuthService */]) === "function" && _a || Object])
], TopBarComponent);

var _a;
//# sourceMappingURL=top-bar.component.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map