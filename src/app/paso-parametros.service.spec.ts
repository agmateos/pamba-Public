import { TestBed, inject } from '@angular/core/testing';

import { PasoParametrosService } from './paso-parametros.service';

describe('PasoParametrosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PasoParametrosService]
    });
  });

  it('should be created', inject([PasoParametrosService], (service: PasoParametrosService) => {
    expect(service).toBeTruthy();
  }));
});
