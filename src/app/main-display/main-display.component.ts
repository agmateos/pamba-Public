import { Component, OnInit } from '@angular/core';
import { ProductosService } from '../productos.service';
import { PasoParametrosService } from '../paso-parametros.service';

import { Producto } from '../app.component';
import { Router, ActivatedRoute, Params  } from '@angular/router';
import { AuthService } from '../auth.service';


import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'main-display',
  templateUrl: './main-display.component.html',
  styleUrls: ['./main-display.component.css']
})

export class MainDisplayComponent implements OnInit {
  errorMessage: string;

  openProduct(id) {
    this.router.navigate(['/product/' + id]);
  }

  constructor(private aService: AuthService,private pService: ProductosService, private ppService: PasoParametrosService, public router: Router, public route: ActivatedRoute) { }

  public user;
  public categories = ''

  ngOnInit() {
    var aProductos: Producto[];
    this.pService.getProductos().subscribe(res => { this.ppService.setProductos(res.json()) });
    //this.pService.getProductos().subscribe(res => {aProductos = res.json()});
    this.ppService.setProductos(aProductos);



    this.route.params // (+) converts string 'id' to a number 
    .switchMap((params: Params) => this.aService.getUsuario('000000000000000000000001')).subscribe(res => {
       console.log(res.json())
       this.user = res.json()
       this.categories =this.user.categories.join(", ");
       console.log(this.categories)
       this.ppService.setCategorias(this.user.categories);
     });



  }
}
