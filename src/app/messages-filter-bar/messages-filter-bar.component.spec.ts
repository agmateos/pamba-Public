import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessagesFilterBarComponent } from './messages-filter-bar.component';

describe('MessagesFilterBarComponent', () => {
  let component: MessagesFilterBarComponent;
  let fixture: ComponentFixture<MessagesFilterBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessagesFilterBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagesFilterBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
