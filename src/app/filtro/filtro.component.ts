import { Component, OnInit, Input } from '@angular/core';
import { ProductosService } from '../productos.service';
import { PasoParametrosService } from '../paso-parametros.service';
import { Producto } from '../app.component';

declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'filtro',
  templateUrl: './filtro.component.html',
  styleUrls: ['./filtro.component.css']
})
export class FiltroComponent implements OnInit {
  @Input() f: any;
  private title = 'Name-test';
  private name = 'Name-test';
  private filtros: String[];
  private type: String;

  public fSeleccionados: String[] = [];

  public checkSelected(filtro: String) {
    if (jQuery('#' + this.name + "_" + filtro).is(":checked")) {
      this.fSeleccionados.push(filtro);
    } else {
      let index = this.fSeleccionados.indexOf(filtro);
      this.fSeleccionados.splice(index, 1);
    }

    var aProductos : Producto[];
    this.pService.setFiltros(this.name, this.fSeleccionados).subscribe(res => {this.ppService.setProductos(res.json())});
    //this.pService.setFiltros(this.name, this.fSeleccionados).subscribe(res => {aProductos = res.json()});
    //this.ppService.setProductos(aProductos);
  }

  init() {
    this.title = this.f.title;
    this.name = this.f.name;
    this.filtros = this.f.filtros;
    this.type = this.f.type;
  }

  constructor(private pService: ProductosService, private ppService: PasoParametrosService) { }


  ngOnInit() {
    console.log("filtro", this.f);
    this.init();
  }

}
