import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: '[product-row]',
  templateUrl: './product-row.component.html',
  styleUrls: ['./product-row.component.css']
})
export class ProductRowComponent implements OnInit {
  @Input() p: any;

  private title = "";
  private provider = "";
  private rating = "";
  private size = "";
  private extension = "";
  private date = "";

  constructor() {
  }
 
  init(){
    this.title = this.p.producto.title;
    this.provider = this.p.producto.provider;
    this.rating = this.p.producto.rating;
    this.size = this.p.producto.size;
    this.extension = this.p.producto.extension;
    this.date = this.p.fecha;
  }

  ngOnInit() {
    this.init();
  }

}
