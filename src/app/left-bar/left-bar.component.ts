import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'left-bar',
  templateUrl: './left-bar.component.html',
  styleUrls: ['./left-bar.component.css']
})
export class LeftBarComponent implements OnInit {
  private title = "Menu principal";
  private currentView = "home";

  navigate(view) {
    this.currentView = view;
    this.router.navigate(['/'+ view]);
  }

  constructor(public router: Router) { }

  ngOnInit() {
  }

}
