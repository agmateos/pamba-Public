exports = module.exports = function(app, mongoose){
    var filtroSchema = new mongoose.Schema({
        title: { type: String },
        units: { type: [String] },    
    },{
        collection: 'filtro-cantiad'
    });

    mongoose.model('FiltroCantidad', filtroSchema);
};
