exports = module.exports = function(app, mongoose){
    var filtroSchema = new mongoose.Schema({
        title: { type: String },
        name: { type: String },        
        filtros: { type: [String] },
        type: { type: String },    
    });

    mongoose.model('Filtro', filtroSchema);
};
