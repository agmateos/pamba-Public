//File: controllers/productos.js
var mongoose = require('mongoose');
var Filtro  = mongoose.model('Filtro');
var FiltroCantidad  = mongoose.model('FiltroCantidad');


//GET - Return all products in the DB
exports.findAllFiltros = function(req, res) {
    Filtro.find(function(err, filtros) {
        if(err) res.send(500, err.message);

        console.log('GET /filtros');
        res.status(200).jsonp(filtros);
    });
};

//GET - Return a Product with specified ID
exports.findFiltroById = function(req, res) {
    Filtro.findById(req.params.id, function(err, filtro) {
        if(err) return res.send(500, err.message);

        console.log('GET /filtro/' + req.params.id);
        res.status(200).jsonp(filtro);
    });
};

//GET - Return all products in the DB
exports.findAllFiltrosCantidad = function(req, res) {
    FiltroCantidad.find(function(err, filtros) {
        if(err) res.send(500, err.message);

        console.log('GET /filtros-cantidad');
        res.status(200).jsonp(filtros);
    });
};

//GET - Return a Product with specified ID
exports.findFiltroCantidadById = function(req, res) {
    FiltroCantidad.findById(req.params.id, function(err, filtro) {
        if(err) return res.send(500, err.message);

        console.log('GET /filtro-cantidad/' + req.params.id);
        res.status(200).jsonp(filtro);
    });
};

/*
//POST - Insert a new filtro-cantidad in the DB
exports.addFiltro = function(req, res) {
    console.log('POST');
    console.log(req.body);

    var filtroCantidad = new FiltroCantidad({
        title:    req.body.title
    });

    filtroCantidad.save(function(err, filtroCantidad) {
        if(err) return res.status(500).send( err.message);
        res.status(200).jsonp(filtroCantidad);
    });
};
*/